grady-index
==

## Documentation

See [gitlab runner](https://gitlab.gwdg.de/grady-corp/grady-docs/docs/gitlab-runner/README.md) on [grady-docs](https://gitlab.gwdg.de/grady-corp/grady-docs) . 



## Before Docker 

This is a small site which displays links to all the running Grady instances.

### Development mode
Simply run
```
yarn dev
```

### Building the site
Simply run
```
yarn build
```

the result will be under `site/.vuepress/dist`