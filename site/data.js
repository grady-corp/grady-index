// Enter new instances here!

const brosenne = 'Dr. Brosenne'
const damm = 'Prof. Dr. Damm'

const info1 = 'Informatik I'
const gdpi = 'Grundlagen der Praktische Informatik'
const ce = 'Technische Informatik'
const os = 'Betriebssysteme'

export const examReview = true
export const examReviewDate = '01.03.2023'

export default [
    {
        lecturer: brosenne,
        course: os,
        year: '2022/23',
        semester: 'Winter',
        link: 'https://grady.informatik.uni-goettingen.de/os-exam/wise2022/',
        review: true,
    }
]

