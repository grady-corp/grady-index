const base = process.env.GRADY_INDEX_BASE || ""

module.exports = {
    title: "Grady Index",
    description: "A small static site with an index of all the hosted Grady instances.",
    base: base
}