FROM node:16.14-alpine

# install simple http server for serving static content
RUN yarn global add http-server

# make the 'app' folder the current working directory
WORKDIR /app

# copy both 'package.json' and 'package-lock.json' (if available)
COPY package.json yarn.lock ./

# install project dependencies
RUN yarn install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY site ./site

# build app for production with minification
RUN yarn build

COPY . .

RUN tar -czvf dist.tar site/.vuepress/dist

EXPOSE 8080
CMD [ "http-server", "site/.vuepress/dist" ]